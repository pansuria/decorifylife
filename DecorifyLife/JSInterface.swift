//
//  JSInterface.swift
//  JSInterface
//
//  Created by Kem on 9/12/15.
//  Copyright © 2015 Kem. All rights reserved.
//

import Foundation
import JavaScriptCore
import UIKit

@objc protocol MyExport : JSExport
{
    func check(_ message : String)
   
    func opendrawer()
}


class JSInterface : NSObject, MyExport
{
    func check(_ message: String) {
        //print("JS Interface works!")
    }
    
    func opendrawer() {
        DispatchQueue.main.async(execute: {
           // print("yup JS Interface works!")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "proceedtoRefresh"),object: self,userInfo: nil)
        })
    }
}
