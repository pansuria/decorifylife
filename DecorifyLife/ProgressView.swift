//
//  ProgressView.swift
//  DecorifyLife
//
//  Created by Administrator on 19/08/19.
//  Copyright © 2019 TMFC. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
func startNVActivity() {
    
    NVActivityIndicatorView.DEFAULT_TYPE = .ballRotateChase
    NVActivityIndicatorView.DEFAULT_COLOR = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    let activityData = ActivityData()
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
}

// MARK : Stop Activity
func stopNVActivity() {
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
}
func ClearAllData(){
    URLCache.shared.removeAllCachedResponses()
    URLCache.shared.diskCapacity = 0
    URLCache.shared.memoryCapacity = 0
    
    let cookieJar = HTTPCookieStorage.shared
    for cookie in cookieJar.cookies! {
        cookieJar.deleteCookie(cookie)
    }
    UserDefaults.standard.removeObject(forKey: "frontendcookies")
    UserDefaults.standard.synchronize()
}
