//
//  ConnectwithusViewController.swift
//  DecorifyLife
//
//  Created by Administrator on 14/08/19.
//  Copyright © 2019 TMFC. All rights reserved.
//

import UIKit

class ConnectwithusViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func btnFacebook(_ sender: Any) {
        let url = URL(string: "https://www.facebook.com/DecorifyLife/")
        UIApplication.shared.open(url!) { (result) in
            if result {
                print(" The URL was delivered successfully!")
            }
        }
    }
    
    @IBAction func btnTwitter(_ sender: Any) {
        let url = URL(string: "https://twitter.com/decorifylife")
        UIApplication.shared.open(url!) { (result) in
            if result {
                print("!")
            }
        }
    }
    @IBAction func btnPinterest(_ sender: Any) {
        let url = URL(string: "https://in.pinterest.com/decorifylife/")
        UIApplication.shared.open(url!) { (result) in
            if result {
                print("!")
            }
        }
    }
    @IBAction func btnInstagram(_ sender: Any) {
        let url = URL(string: "https://www.instagram.com/decorifylife/")
        UIApplication.shared.open(url!) { (result) in
            if result {
                print("!")
            }
        }
    }
    @IBAction func btnMenu(_ sender: Any) {
        
        self.performSegue(withIdentifier: "socialtomain", sender: nil)
    }
}
