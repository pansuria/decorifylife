

import UIKit

var BASEURL = "https://www.decorifylife.com/"
class LeftViewController : UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
   let darkView = UIView()
    var menu = [["Home","Browse by Category","Order History","My Wish List","My Account","Connect With Us","Newsletter","Blog"],["Contact US","FAQ"],["Return Policy","Privacy Policy","Terms & Conditions"],["Settings","Give Feedback","Rate US","Log Out"]]
   
    //link
    var menulink = [["https://www.decorifylife.com/","Browse by Category","https://www.decorifylife.com/customer/account","https://www.decorifylife.com/wishlist/","https://www.decorifylife.com/customer/account/","Connect With Us","https://www.decorifylife.com/newsletter/manage/","https://www.decorifylife.com/blog/"],["https://www.decorifylife.com/contact-us","https://www.decorifylife.com/faq"],["https://www.decorifylife.com/return_policy","https://www.decorifylife.com/privacy_policy","https://www.decorifylife.com/terms_condition"],["Settings","Give Feedback","Rate US","https://www.decorifylife.com/customer/account/logoutSuccess/"]]
    var sections = ["","Support","Legal","More"]
    let kHeaderSectionTag: Int = 6900;
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []
    var sectionrowclick : Bool = false
    var selectedCellIndexPath: IndexPath?
    var yindex = 45
    var i = 0
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
       //self.tableView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      
    }
    override func viewWillAppear(_ animated: Bool) {
        refreshtableview()
        darkView.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        darkView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        darkView.frame = self.revealViewController().frontViewController.view.bounds
        self.revealViewController().frontViewController.view.addSubview(darkView)
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        BASEURL = "https://www.decorifylife.com/customer/account"
        self.performSegue(withIdentifier: "home", sender: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        darkView.removeFromSuperview()
    }
    func refreshtableview(){
        menu = [["Home","Browse by Category","Order History","My Wish List","My Account","Connect With Us","Newsletter","Blog"],["Contact US","FAQ"],["Return Policy","Privacy Policy","Terms & Conditions"],["Settings","Give Feedback","Rate US",userlogin]]
        
        var loginlink = ""
        if userlogin == "Log In"{
            loginlink = "https://www.decorifylife.com/customer/account/login/"
        }else{
            loginlink = "https://www.decorifylife.com/customer/account/logoutSuccess/"
        }
        
        menulink = [["https://www.decorifylife.com/","Browse by Category","https://www.decorifylife.com/customer/account","https://www.decorifylife.com/wishlist/","https://www.decorifylife.com/customer/account/","Connect With Us","https://www.decorifylife.com/newsletter/manage/","https://www.decorifylife.com/blog/"],["https://www.decorifylife.com/contact-us","https://www.decorifylife.com/faq"],["https://www.decorifylife.com/return_policy","https://www.decorifylife.com/privacy_policy","https://www.decorifylife.com/terms_condition"],["Settings","Give Feedback","Rate US",loginlink]]
        
        self.tableView.reloadData()
    }
}


extension LeftViewController : UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return self.menu[section].count
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
       return self.sections.count
    }
  
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
       
        return self.sections[section]
       
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        
        return 45
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }

     func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.textLabel?.textColor = #colorLiteral(red: 1, green: 0, blue: 0.5544145107, alpha: 1)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailCell
     
        cell.lblTitle.text = self.menu[indexPath.section][indexPath.row]
   
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        BASEURL = self.menulink[indexPath.section][indexPath.row]
        if indexPath.section == 0 && indexPath.row == 1{
            self.performSegue(withIdentifier: "browse", sender: nil)
        }else if indexPath.section == 0 && indexPath.row == 5{
            self.performSegue(withIdentifier: "connectwithus", sender: nil)
        }else if indexPath.section == 3 && indexPath.row == 3{
            if userlogin == "Log In"{
                startNVActivity()
                self.performSegue(withIdentifier: "home", sender: nil)
            }else{
                startNVActivity()
                ClearAllData()
                self.performSegue(withIdentifier: "home", sender: nil)
            
            }
        }else if indexPath.section == 3 && indexPath.row == 2{
            rateApp()
        } else{
            startNVActivity()
            self.performSegue(withIdentifier: "home", sender: nil)
        }
    }
    func rateApp() {
        guard let url = URL(string: "itms-apps://itunes.apple.com/app/" + "id650762525") else {
            return
        }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
class DetailCell: UITableViewCell {
    
    @IBOutlet weak var btnReject: UIButton!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var lblchild: UILabel!
    @IBOutlet weak var childview: UIView!
    
    @IBOutlet weak var lblduration: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    /* var objLeave : Leave?{
     didSet{
     lblDate.text = (objLeave?.FromDate)! + " To " + (objLeave?.ToDate)!
     lblduration.text = objLeave?.Duration
     lblType.text = objLeave?.LeaveType
     lblDescription.text = objLeave?.Reason
     }
     }*/
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
