//
//  BrowseViewController.swift
//  DecorifyLife
//
//  Created by Administrator on 13/08/19.
//  Copyright © 2019 TMFC. All rights reserved.
//

import UIKit

class BrowseViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource {

    var menu = [["Cushions","Flooring","Curtains","Bath Linen & Accessories"],
                ["Bed Sheets","Kids Bed Sheets","Blankets & Quilts","Kids Blankets & Quilts" , "Bedding Sets","Comforters","Pillows","Diwan Sets"],
                ["Trays & Platters","Bowls","Kitchen Decor","Coasters","Table Accessories"],
                ["Wall Lights","Ceiling Lights","Lantern","Hanging Lights" , "Floor Lamps","Table Lamps"] ,
                ["Wall Accents","Wall Art","Door Decor","Clocks & Time Pieces","Handcrafted Products","Figurine & Statue"],
                ["Vases","Garden"],
                []]
    
     var sections = ["Living","Bedroom","Kitchen & Dining","Lamps & Lights","Decor","Pots & Planters","Gifts & Others"]
     @IBOutlet var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        BASEURL = "https://www.decorifylife.com/"
    }
    //MARK:- CollectionView method
    
     func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menu[section].count
    }
  
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath as IndexPath) as! CollectionViewCell
        
       cell.displayContent(title: self.menu[indexPath.section][indexPath.row])
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
     //   print("You selected cell #\(indexPath.item)!")
         DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10)) {
            startNVActivity()
        }
        BASEURL = BASEURL + self.sections[indexPath.section].lowercased() + "/" + self.menu[indexPath.section][indexPath.row].lowercased()
        setbaseurl()
        print(BASEURL)
        self.performSegue(withIdentifier: "reveal", sender: nil)
    }
   
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as? SectionHeader{
            sectionHeader.aView.layer.cornerRadius = 5
            sectionHeader.aView.clipsToBounds = true
            sectionHeader.sectionHeaderlabel.text = self.sections[indexPath.section]
            sectionHeader.btnSection.tag = indexPath.section
            sectionHeader.btnSection.addTarget(self, action: #selector(btnsectionclick(sender:)), for: .touchUpInside)
           // sectionHeader.Headerinit()
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    @objc func btnsectionclick(sender : UIButton){
        BASEURL = BASEURL + self.sections[sender.tag].lowercased()
        setbaseurl()
        self.performSegue(withIdentifier: "reveal", sender: nil)
    }
    func setbaseurl(){
        BASEURL = BASEURL.replacingOccurrences(of: "lamps & lights", with: "lights")
        BASEURL = BASEURL.replacingOccurrences(of: "kids blankets & quilts", with: "kids blankets")
        BASEURL = BASEURL.replacingOccurrences(of: " & ", with: "-")
        BASEURL = BASEURL.replacingOccurrences(of: " ", with: "-")
        BASEURL = BASEURL.replacingOccurrences(of: "decor/handcrafted-products", with: "handcrafted-products")
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        return CGSize(width: self.view.frame.size.width - 40, height: 45)
    }
}
extension BrowseViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
        return CGSize(width: self.collectionView.frame.size.width / 3 - 8, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width:collectionView.frame.size.width, height:70)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1)
    }
}
class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var aView: UIView!
    func displayContent( title: String) {
        lbltitle.text = title
      
        aView.dropShadow()
        aView.layer.cornerRadius = 5
        aView.clipsToBounds = true
        
        contentView.layer.cornerRadius = 5.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath
    }
}

class SectionHeader: UICollectionReusableView {
    @IBOutlet weak var sectionHeaderlabel: UILabel!
    @IBOutlet var aView: UIView!
    @IBOutlet weak var btnSection: UIButton!
    func Headerinit(){
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 0.5
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = true
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.layer.cornerRadius).cgPath
    }
}
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
