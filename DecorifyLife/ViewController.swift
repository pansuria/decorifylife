//
//  ViewController.swift
//  DecorifyLife
//
//  Created by Administrator on 13/08/19.
//  Copyright © 2019 TMFC. All rights reserved.
//

import UIKit
extension HTTPCookiePropertyKey {
    static let HttpOnly = HTTPCookiePropertyKey("HttpOnly")
}
var userlogin = "Log In"
class ViewController: UIViewController ,UIWebViewDelegate{

    @IBOutlet weak var aViewInternet: UIView!
    @IBOutlet weak var btnAgain: UIButton!
    @IBOutlet weak var webView : UIWebView!
    var refController:UIRefreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.aViewInternet.isHidden = true
        let notificationName = Notification.Name("proceedtoRefresh")
        NotificationCenter.default.addObserver(self, selector: #selector(proceedtoRefresh), name: notificationName, object: nil)
        self.webView.delegate = self
        HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
        /****/
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5)) {
            if !BASEURL.contains("http"){
                BASEURL = "https://www.decorifylife.com/"
            }
            if Reachability.isConnectedToNetwork() == true {
                //startNVActivity()
                self.loadwebview()
            } else {
                 stopNVActivity()
                 self.aViewInternet.isHidden = false
                //UIAlertView.init(title: "", message: "Internet connection failed", delegate: nil, cancelButtonTitle: "OK").show()
            }
        }
        btnAgain.layer.cornerRadius = 22.5
        btnAgain.clipsToBounds = true
    }
    @IBAction func btnTryAgain(_ sender: Any) {
         self.aViewInternet.isHidden = true
          if Reachability.isConnectedToNetwork() == true {
            self.loadwebview()
          }else{
            stopNVActivity()
            self.aViewInternet.isHidden = false
        }
    }
    func loadwebview(){
        
        //startNVActivity()
        let urlDeBase = BASEURL
        if let url = URL(string: urlDeBase){
            do {
               /* let myHTML = try String(contentsOf: url, encoding: .utf8)
                if myHTML.contains("Log In"){
                    userlogin = "Log In"
                }else{
                    userlogin = "Log Out"
                }
                print(userlogin)*/
              /*  var mystring = myHTML.replacingOccurrences(of: "<div class=\"ves-container no-margin\" style=\"padding:50px 0\">", with: "<div class=\"ves-container no-margin\" style=\"display:none\">")
                mystring = mystring.replacingOccurrences(of: "<div class=\"row-inner clearfix wrapper\" >", with: "<div class=\"row-inner clearfix wrapper\" style=\"display:none\">")
                mystring = mystring.replacingOccurrences(of: "<div class=\"ves-container powered\" >", with: "<div class=\"ves-container powered\" style=\"display:none\">")*/
                
                self.webView.addJavascriptInterface(JSInterface(), forKey: "Andy");
              
                let userDefaults = UserDefaults.standard
                if let cookieDictionary = userDefaults.dictionary(forKey: "frontendcookies") {
                   // setCookie(key: "frontend", value: cookieDictionary["frontend"] as! String)
                }
               // webView.loadHTMLString(mystring, baseURL: url)
                var requestObj = URLRequest(url: url)
                requestObj.cachePolicy = .returnCacheDataElseLoad
                webView.loadRequest(requestObj)
                webView.scrollView.showsVerticalScrollIndicator = false
                
                refController.bounds = CGRect(x: 0, y: 50, width: refController.bounds.size.width, height: refController.bounds.size.height)
                refController.addTarget(self, action: #selector(mymethodforref(refresh:)), for: UIControl.Event.valueChanged)
                //refController.attributedTitle = NSAttributedString(string: "Pull to refresh")
                webView.scrollView.addSubview(refController)
             
            }
            catch let error {
                if Reachability.isConnectedToNetwork() == true {
                    BASEURL = "https://www.decorifylife.com/"
                    loadwebview()
                } else {
                     stopNVActivity()
                    self.aViewInternet.isHidden = false
                   // UIAlertView.init(title: "", message: "Internet connection failed", delegate: nil, cancelButtonTitle: "OK").show()
                }
            }
        }else{
            print("error")
        }
    }
    @objc func mymethodforref(refresh:UIRefreshControl){
        //startNVActivity()
        if Reachability.isConnectedToNetwork() == true {
            webView.reload()
            refController.endRefreshing()
        } else {
            stopNVActivity()
            self.aViewInternet.isHidden = false
        }
    }
    @objc func proceedtoRefresh(notification: NSNotification) -> Void {
        
        revealViewController()?.rearViewRevealWidth = self.view.frame.size.width - 70
        self.revealViewController().revealToggle(self)
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        print(request.url!.absoluteString)
        if Reachability.isConnectedToNetwork() == true {
            if request.url?.query != nil {
                print("\(request.url!.query!)")
            }
            if request.url!.absoluteString == "https://www.decorifylife.com/customer/account/logout/"{
                //webView.stringByEvaluatingJavaScript(from: "localStorage.clear();")
                ClearAllData()
            }
        }else{
            stopNVActivity()
            self.aViewInternet.isHidden = false
        }
        return true
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        startNVActivity()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("finish")
        
        stopNVActivity()
        if Reachability.isConnectedToNetwork() == true {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            let id : String = webView.stringByEvaluatingJavaScript(from: "document.getElementById('Id').value")!
            print(id)
            
            let myHTML = webView.stringByEvaluatingJavaScript(from:"document.documentElement.outerHTML")
            // print(myHTML)
            if myHTML!.contains("Log In"){
                //ClearAllData()
                userlogin = "Log In"
            }else{
                userlogin = "Log Out"
            }
            storeCookies()
        } else {
            stopNVActivity()
            self.aViewInternet.isHidden = false
        }
    }
   
    func webView(_ webView: UIWebView, didFailLoadWithError error: Swift.Error) {
         if Reachability.isConnectedToNetwork() == true {
            stopNVActivity()
         } else {
            stopNVActivity()
            self.aViewInternet.isHidden = false
        }
    }
    deinit {
        self.webView.removeJavascriptInterfaces()
    }
    func setCookie(key: String, value: String) {
        let URL = "www.decorifylife.com"//https://www.decorifylife.com
        let ExpTime = TimeInterval(60 * 60 * 24 * 365)
       // print(key , value)
        HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
        let cookieProps: [HTTPCookiePropertyKey : Any] = [
            HTTPCookiePropertyKey.domain: URL,
            HTTPCookiePropertyKey.path: "/",
            HTTPCookiePropertyKey.name: key,
            HTTPCookiePropertyKey.value: value,
           // HTTPCookiePropertyKey.secure: "TRUE",
            HTTPCookiePropertyKey.expires: NSDate(timeIntervalSinceNow: ExpTime)
        ]
        if let cookie = HTTPCookie(properties: cookieProps) {
            //print(cookie)
            HTTPCookieStorage.shared.setCookie(cookie)
        }
        //  HTTPCookiePropertyKey.HttpOnly: true,
        // HTTPCookiePropertyKey.secure: "TRUE",
    }
    
    //Cookie
    func storeCookies() {
        HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
        let cookiesStorage = HTTPCookieStorage.shared
        let userDefaults = UserDefaults.standard
        
       let serverBaseUrl = "https://www.decorifylife.com"
        
        var cookieDict = [String : AnyObject]()
        
        for cookie in cookiesStorage.cookies(for: NSURL(string: serverBaseUrl)! as URL)! {
            cookieDict[cookie.name] = cookie.properties as AnyObject?
            if cookie.name == "frontend"{
               // print(cookieDict)
                userDefaults.set([cookie.name : cookie.value], forKey: "frontendcookies")
            }
        }
    }
    func restoreCookies() {
        let cookiesStorage = HTTPCookieStorage.shared
        let userDefaults = UserDefaults.standard
        
        if let cookieDictionary = userDefaults.dictionary(forKey: "frontendcookies") {
            
            for (_, cookieProperties) in cookieDictionary {
                if let cookie = HTTPCookie(properties: cookieProperties as! [HTTPCookiePropertyKey : Any] ) {
                    cookiesStorage.setCookie(cookie)
                    HTTPCookieStorage.shared.cookieAcceptPolicy = HTTPCookie.AcceptPolicy.always
                }
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if userlogin == "Log In"{
            ClearAllData()
        }
    }
}

/*
 let ud: UserDefaults = UserDefaults.standard
 let data: NSData? = ud.object(forKey: "cookie") as? NSData
 if let cookie = data {
 let datas: NSArray? = NSKeyedUnarchiver.unarchiveObject(with: cookie as Data) as? NSArray
 if let cookies = datas {
 for c in cookies as! [HTTPCookie] {
 HTTPCookieStorage.shared.setCookie(c)
 }
 }
 }
 
 /***
 let cookieJar: HTTPCookieStorage = HTTPCookieStorage.shared
 let data: NSData = NSKeyedArchiver.archivedData(withRootObject: cookieJar.cookies) as NSData
 let ud: UserDefaults = UserDefaults.standard
 ud.set(data, forKey: "cookie")
 ***/
 
 extension HTTPCookiePropertyKey {
 static let HttpOnly = HTTPCookiePropertyKey("HttpOnly")
 }
 HTTPCookiePropertyKey.HttpOnly: true,
 */
